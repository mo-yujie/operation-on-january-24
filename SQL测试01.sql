/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50636
Source Host           : localhost:3306
Source Database       : sql_test

Target Server Type    : MYSQL
Target Server Version : 50636
File Encoding         : 65001

Date: 2020-09-18 17:36:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for 01_t_account
-- ----------------------------
DROP TABLE IF EXISTS `01_t_account`;
CREATE TABLE `01_t_account` (
  `account_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '所属账户id',
  `account_name` varchar(255) NOT NULL COMMENT '账户名',
  `password` varchar(16) NOT NULL COMMENT '密码',
  `last_login` timestamp NOT NULL DEFAULT '1979-01-01 00:00:00' COMMENT '最后登录时间',
  `point` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '积分',
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 01_t_account
-- ----------------------------
INSERT INTO `01_t_account` VALUES ('1', 'jack', '12345', '2020-09-17 20:08:00', '8');
INSERT INTO `01_t_account` VALUES ('2', 'seemygo', '12345', '2020-09-17 19:00:00', '45');
INSERT INTO `01_t_account` VALUES ('3', 'wolfcode02', '12345', '2020-09-17 20:02:00', '72');
INSERT INTO `01_t_account` VALUES ('4', 'tomcat123', '12345', '2020-09-16 10:15:00', '65');
INSERT INTO `01_t_account` VALUES ('5', 'spring', '12345', '2020-09-15 15:00:00', '23');
INSERT INTO `01_t_account` VALUES ('6', 'shiro', '12345', '2020-09-17 10:18:00', '3232');
INSERT INTO `01_t_account` VALUES ('7', 'springcloud', '12345', '2020-09-17 17:56:00', '545');
INSERT INTO `01_t_account` VALUES ('8', 'dubbo', '12345', '2020-09-14 09:59:00', '12');
INSERT INTO `01_t_account` VALUES ('9', 'wolfcode01', '12345', '2020-09-04 02:00:00', '767');
INSERT INTO `01_t_account` VALUES ('10', 'rockmq', '12345', '2020-09-17 23:23:00', '99');

-- ----------------------------
-- Table structure for 01_t_account_nickname
-- ----------------------------
DROP TABLE IF EXISTS `01_t_account_nickname`;
CREATE TABLE `01_t_account_nickname` (
  `account_id` int(10) unsigned NOT NULL COMMENT '所属账户id',
  `account_nickname` varchar(50) NOT NULL COMMENT '昵称',
  PRIMARY KEY (`account_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 01_t_account_nickname
-- ----------------------------
INSERT INTO `01_t_account_nickname` VALUES ('1', '叩丁狼教育');
INSERT INTO `01_t_account_nickname` VALUES ('2', '小码哥教育');
INSERT INTO `01_t_account_nickname` VALUES ('3', 'tomcat猫');
INSERT INTO `01_t_account_nickname` VALUES ('4', '大飞');
INSERT INTO `01_t_account_nickname` VALUES ('5', '雄威');
INSERT INTO `01_t_account_nickname` VALUES ('6', '东风一号');
INSERT INTO `01_t_account_nickname` VALUES ('7', '东风一号');
INSERT INTO `01_t_account_nickname` VALUES ('8', '小惠');
INSERT INTO `01_t_account_nickname` VALUES ('9', '老钟');
INSERT INTO `01_t_account_nickname` VALUES ('10', '李想');

-- ----------------------------
-- Table structure for 02_t_worker
-- ----------------------------
DROP TABLE IF EXISTS `02_t_worker`;
CREATE TABLE `02_t_worker` (
  `code` varchar(2) NOT NULL COMMENT '编码',
  `name` varchar(30) NOT NULL COMMENT '姓名',
  `department` varchar(10) NOT NULL COMMENT '部门',
  `parday` varchar(10) NOT NULL COMMENT '发薪日期，存储格式 yyyy/mm/dd',
  `payment` decimal(14,2) NOT NULL COMMENT '薪水'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 02_t_worker
-- ----------------------------
INSERT INTO `02_t_worker` VALUES ('4', '小码哥', '技术', '2015/04/11', '1300.00');
INSERT INTO `02_t_worker` VALUES ('2', '叩丁狼', '市场', '2015/04/11', '2000.00');
INSERT INTO `02_t_worker` VALUES ('3', '叩丁狼', '市场', '2015/05/11', '1000.00');
INSERT INTO `02_t_worker` VALUES ('1', '大飞', '市场', '2015/04/11', '1000.00');
INSERT INTO `02_t_worker` VALUES ('5', '叩丁狼', '市场', '2015/04/19', '1300.00');
INSERT INTO `02_t_worker` VALUES ('6', '雄威', '技术', '2018/04/11', '1300.00');
INSERT INTO `02_t_worker` VALUES ('7', '小惠', '技术', '2018/04/19', '1300.00');
INSERT INTO `02_t_worker` VALUES ('8', '老钟', '人事', '2018/04/19', '2300.00');
INSERT INTO `02_t_worker` VALUES ('9', '杨龙', '人事', '2018/04/19', '1500.00');
INSERT INTO `02_t_worker` VALUES ('10', '杨龙', '人事', '2018/04/11', '1500.00');
INSERT INTO `02_t_worker` VALUES ('11', '杨龙', '人事', '2018/04/20', '2000.00');

-- ----------------------------
-- Table structure for 03_t_score
-- ----------------------------
DROP TABLE IF EXISTS `03_t_score`;
CREATE TABLE `03_t_score` (
  `student_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `score` int(11) NOT NULL,
  PRIMARY KEY (`student_id`,`subject`,`score`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 03_t_score
-- ----------------------------
INSERT INTO `03_t_score` VALUES ('1', '历史', '99');
INSERT INTO `03_t_score` VALUES ('1', '英语', '97');
INSERT INTO `03_t_score` VALUES ('1', '语文', '85');
INSERT INTO `03_t_score` VALUES ('2', '数学', '90');
INSERT INTO `03_t_score` VALUES ('2', '语文', '90');
INSERT INTO `03_t_score` VALUES ('3', '地理', '68');
INSERT INTO `03_t_score` VALUES ('3', '语文', '82');

-- ----------------------------
-- Table structure for 03_t_student
-- ----------------------------
DROP TABLE IF EXISTS `03_t_student`;
CREATE TABLE `03_t_student` (
  `student_id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 03_t_student
-- ----------------------------
INSERT INTO `03_t_student` VALUES ('1', '小明', '9', '男');
INSERT INTO `03_t_student` VALUES ('2', '小红', '10', '女');
INSERT INTO `03_t_student` VALUES ('3', '小李', '11', '男');

-- ----------------------------
-- Table structure for 04_t_course
-- ----------------------------
DROP TABLE IF EXISTS `04_t_course`;
CREATE TABLE `04_t_course` (
  `c_no` int(11) NOT NULL,
  `c_name` varchar(255) DEFAULT NULL,
  `t_no` int(11) DEFAULT NULL,
  PRIMARY KEY (`c_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 04_t_course
-- ----------------------------
INSERT INTO `04_t_course` VALUES ('1', '语文', '1');
INSERT INTO `04_t_course` VALUES ('2', '数学', '4');
INSERT INTO `04_t_course` VALUES ('3', '英语', '3');
INSERT INTO `04_t_course` VALUES ('4', '历史', '2');

-- ----------------------------
-- Table structure for 04_t_sc
-- ----------------------------
DROP TABLE IF EXISTS `04_t_sc`;
CREATE TABLE `04_t_sc` (
  `s_no` int(11) NOT NULL,
  `c_no` int(11) NOT NULL,
  `score` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`s_no`,`c_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 04_t_sc
-- ----------------------------
INSERT INTO `04_t_sc` VALUES ('1', '1', '99');
INSERT INTO `04_t_sc` VALUES ('1', '2', '88');
INSERT INTO `04_t_sc` VALUES ('1', '3', '85');
INSERT INTO `04_t_sc` VALUES ('1', '4', '85');
INSERT INTO `04_t_sc` VALUES ('2', '1', '30');
INSERT INTO `04_t_sc` VALUES ('2', '2', '40');
INSERT INTO `04_t_sc` VALUES ('2', '3', '30');
INSERT INTO `04_t_sc` VALUES ('2', '4', '60');
INSERT INTO `04_t_sc` VALUES ('3', '1', '80');
INSERT INTO `04_t_sc` VALUES ('3', '2', '30');
INSERT INTO `04_t_sc` VALUES ('3', '3', '40');

-- ----------------------------
-- Table structure for 04_t_student
-- ----------------------------
DROP TABLE IF EXISTS `04_t_student`;
CREATE TABLE `04_t_student` (
  `s_no` int(11) NOT NULL,
  `sname` varchar(255) DEFAULT NULL,
  `sage` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`s_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 04_t_student
-- ----------------------------
INSERT INTO `04_t_student` VALUES ('1', '张三', '10', '男');
INSERT INTO `04_t_student` VALUES ('2', '李四', '11', '女');
INSERT INTO `04_t_student` VALUES ('3', '王五 ', '12', '男');

-- ----------------------------
-- Table structure for 04_t_teacher
-- ----------------------------
DROP TABLE IF EXISTS `04_t_teacher`;
CREATE TABLE `04_t_teacher` (
  `t_no` int(11) NOT NULL,
  `t_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`t_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 04_t_teacher
-- ----------------------------
INSERT INTO `04_t_teacher` VALUES ('1', '李月梅');
INSERT INTO `04_t_teacher` VALUES ('2', '李想');
INSERT INTO `04_t_teacher` VALUES ('3', '程明');
INSERT INTO `04_t_teacher` VALUES ('4', '张倩');

-- ----------------------------
-- Table structure for 05_mz_zy_cost
-- ----------------------------
DROP TABLE IF EXISTS `05_mz_zy_cost`;
CREATE TABLE `05_mz_zy_cost` (
  `pId` int(11) NOT NULL,
  `orgCode` varchar(255) DEFAULT NULL,
  `cost` decimal(10,2) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `indate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 05_mz_zy_cost
-- ----------------------------
INSERT INTO `05_mz_zy_cost` VALUES ('1', 'ZSDX', '300.00', '0', '2020-08-01 20:12:27');
INSERT INTO `05_mz_zy_cost` VALUES ('1', 'ZSDX', '700.00', '0', '2020-08-26 13:44:27');
INSERT INTO `05_mz_zy_cost` VALUES ('1', 'ZSDX', '20.00', '1', '2020-09-17 17:21:33');
INSERT INTO `05_mz_zy_cost` VALUES ('1', 'ZSDX', '12000.00', '1', '2020-09-18 15:23:12');
INSERT INTO `05_mz_zy_cost` VALUES ('1', 'ZSDX', '17000.00', '0', '2020-09-17 17:12:27');
INSERT INTO `05_mz_zy_cost` VALUES ('2', 'ZSDX', '78.00', '1', '2020-09-16 22:00:57');
INSERT INTO `05_mz_zy_cost` VALUES ('3', 'ZSDX', '30000.00', '1', '2020-09-09 15:01:32');
INSERT INTO `05_mz_zy_cost` VALUES ('2', 'ZSDX', '168.00', '0', '2020-09-16 22:00:57');

-- ----------------------------
-- Table structure for 05_t_person
-- ----------------------------
DROP TABLE IF EXISTS `05_t_person`;
CREATE TABLE `05_t_person` (
  `pId` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  PRIMARY KEY (`pId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 05_t_person
-- ----------------------------
INSERT INTO `05_t_person` VALUES ('1', '叩丁狼', '3');
INSERT INTO `05_t_person` VALUES ('2', '小码哥', '5');
INSERT INTO `05_t_person` VALUES ('3', '小明', '6');
INSERT INTO `05_t_person` VALUES ('4', '小强', '20');

-- ----------------------------
-- Table structure for 06_t_sys_log
-- ----------------------------
DROP TABLE IF EXISTS `06_t_sys_log`;
CREATE TABLE `06_t_sys_log` (
  `log_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `login_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 06_t_sys_log
-- ----------------------------

-- ----------------------------
-- Table structure for 06_t_sys_user
-- ----------------------------
DROP TABLE IF EXISTS `06_t_sys_user`;
CREATE TABLE `06_t_sys_user` (
  `user_id` bigint(11) NOT NULL,
  `login_id` bigint(11) NOT NULL,
  `last_login_time` datetime DEFAULT NULL,
  `login_count` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 06_t_sys_user
-- ----------------------------

-- ----------------------------
-- Table structure for 07_t_order_sale
-- ----------------------------
DROP TABLE IF EXISTS `07_t_order_sale`;
CREATE TABLE `07_t_order_sale` (
  `年份` varchar(255) DEFAULT NULL,
  `季度` int(11) DEFAULT NULL,
  `销量` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 07_t_order_sale
-- ----------------------------
INSERT INTO `07_t_order_sale` VALUES ('2018', '1', '11');
INSERT INTO `07_t_order_sale` VALUES ('2018', '2', '12');
INSERT INTO `07_t_order_sale` VALUES ('2018', '3', '13');
INSERT INTO `07_t_order_sale` VALUES ('2018', '4', '14');
INSERT INTO `07_t_order_sale` VALUES ('2019', '1', '21');
INSERT INTO `07_t_order_sale` VALUES ('2019', '2', '22');
INSERT INTO `07_t_order_sale` VALUES ('2019', '3', '23');
INSERT INTO `07_t_order_sale` VALUES ('2019', '4', '24');

-- ----------------------------
-- Table structure for 08_t_game
-- ----------------------------
DROP TABLE IF EXISTS `08_t_game`;
CREATE TABLE `08_t_game` (
  `date` date DEFAULT NULL,
  `result` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 08_t_game
-- ----------------------------
INSERT INTO `08_t_game` VALUES ('2020-09-03', '胜');
INSERT INTO `08_t_game` VALUES ('2020-09-03', '胜');
INSERT INTO `08_t_game` VALUES ('2020-09-03', '负');
INSERT INTO `08_t_game` VALUES ('2020-09-04', '负');
INSERT INTO `08_t_game` VALUES ('2020-09-04', '负');
INSERT INTO `08_t_game` VALUES ('2020-09-06', '负');
INSERT INTO `08_t_game` VALUES ('2020-09-06', '胜');
INSERT INTO `08_t_game` VALUES ('2020-09-06', '胜');
INSERT INTO `08_t_game` VALUES ('2020-09-06', '胜');
