#### MySQL练习题一:

表名: 01_t_account  用户信息表

| 字段         | 描述         |
| ------------ | ------------ |
| account_id   | 所属用户id   |
| account_name | 账户名       |
| password     | 密码         |
| last_login   | 最后登录时间 |
| point        | 积分         |

表名: 01_t_account_nickname 用户昵称表

| 字段             | 描述       |
| ---------------- | ---------- |
| account_id       | 所属账户id |
| account_nickname | 昵称       |



1.请写出最近3个登录的账户

```sql
	select * from 01_t_account ORDER BY last_login desc limit 3
```

执行结果:

![1600333298216](图片/1600333298216.png)

2.查询如下结果:第一列账户ID,第二轮账户名,第三列昵称,第四列积分

```sql
	select a.account_id,a.account_name,an.account_nickname , a.point from 01_t_account a JOIN 01_t_account_nickname an ON a.account_id = an.account_id 
```

执行结果:

![1600333458066](图片/1600333458066.png)

3.对账户名里包含rock的账户，积分修改为0

```sql
	update 01_t_account set point = 0  where account_name like '%rock%'
```

4.查询出重复的昵称

```sql
	select account_id,account_nickname from 01_t_account_nickname GROUP BY account_nickname having count(*)>1
```

执行结果:

![1600333520251](图片/1600333520251.png)

#### MySQL练习题二:

表名:02_t_worker 员工薪资表

| 字段       | 描述               |
| ---------- | ------------------ |
| code       | 编码               |
| name       | 姓名               |
| department | 部门               |
| parday     | 发薪日期（字符串） |
| payment    | 薪水               |



4.查出2015年4月发薪水最多的部门

```sql
select department,sum(payment) from 02_t_worker where  MONTH(parday) = 4 and YEAR(parday)=2015 GROUP BY department ORDER BY sum(payment) desc limit 1
```

执行结果:

![1600334776097](图片/1600334776097.png)

5.查出所有曾经1个月发薪水1次以上的人员的编码，姓名，发薪年月，发薪次数，发薪总额

```sql
select name,parday,COUNT(parday),sum(payment) from 02_t_worker GROUP BY name having COUNT(parday)>1 
```

执行结果:

![1600334856962](图片/1600334856962.png)

#### MySQL练习题三:

表名: 03_t_score 成绩表

| 字段       | 描述     |
| ---------- | -------- |
| student_id | 学生id   |
| subject    | 课程名称 |
| socre      | 分数     |

![1600323820849](图片/1600323820849.png)

表名:03_t_student 学生信息表

| 字段       | 描述   |
| ---------- | ------ |
| student_id | 学生id |
| name       | 姓名   |
| age        | 年龄   |
| gender     | 性别   |



![1600323837936](图片/1600323837936.png)

6.求出该班级语文平均分

```sql
select avg(score) 语文平均成绩 from 03_t_score where subject='语文'
```

![1600335128930](图片/1600335128930.png)

7.求出该班男生各科成绩的平均分

```sql
select  03_t_score.subject,avg(score)  from 03_t_student JOIN 03_t_score ON 03_t_student.student_id = 03_t_score.student_id where 03_t_student.gender='男' GROUP BY 03_t_score.subject  
```

8.统计该班语文成绩高于85分男生人数

```sql
select count(*) from 03_t_student 
JOIN 03_t_score 
on 03_t_student.student_id = 03_t_score.student_id
where 03_t_student.gender='男'  and 03_t_score.subject='语文' and 03_t_score.score>=85
```

![1600335203401](图片/1600335203401.png)

9.统计年龄大于9岁的各科学生人数

```sql
SELECT
	t1.SUBJECT '课程',
	sum(IF(t2.age > 9, 1, 0)) '人数'
FROM
	03_t_score t1,
	03_t_student t2
WHERE
	t1.student_id = t2.student_id
GROUP BY
	t1. SUBJECT
```

![1600335352930](图片/1600335352930.png)

10.求出语数英3科有2科大于90分的同学信息（姓名，年龄，性别）**扩展题**

```sql
select name,age,gender from 03_t_student a
join
(select * from 03_t_score where score>90) b
on a.student_id = b.student_id
GROUP BY name
```

![1600335404580](图片/1600335404580.png)

